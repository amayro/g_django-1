from datetime import timedelta

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _


class UserStatus(models.Model):
    """ShopUser status"""
    class Meta:
        verbose_name = _('User status')
        verbose_name_plural = _('Users statuses')
        ordering = ('sort',)

    DEFAULT = 'df'
    BRONZE = 'br'
    SILVER = 'sl'
    GOLD = 'gl'

    Status_KIND_CHOICES = (
        (DEFAULT, 'Default'),
        (BRONZE, 'Bronze'),
        (SILVER, 'Silver'),
        (GOLD, 'Gold'),
    )

    description = models.TextField(_('description'), blank=True)
    status = models.CharField(max_length=2, choices=Status_KIND_CHOICES, default=DEFAULT)
    sort = models.IntegerField(_('sort'), default=0, null=True, blank=True)

    def __str__(self):
        return f'{self.get_status_display()}'


def default_user_status():
    return UserStatus.objects.get(status=UserStatus.DEFAULT).pk


class ShopUser(AbstractUser):
    avatar = models.ImageField(upload_to='users_avatars', blank=True, default='users_avatars/default_avatar.jpg')
    age = models.PositiveIntegerField(verbose_name='age', null=True)
    activation_key = models.CharField(verbose_name='confirmation key', max_length=128, blank=True)
    status = models.ForeignKey(UserStatus, verbose_name=_(u'Customer status'), related_name='users',
                               default=default_user_status(), on_delete=models.SET_DEFAULT)

    def is_activation_key_expired(self):
        return now() >= self.date_joined + timedelta(hours=48)