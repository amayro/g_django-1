import os

from django.conf import settings
from django.contrib import auth
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import HttpResponseRedirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, UpdateView, DeleteView, RedirectView

from authapp.forms import ShopUserLoginForm, ShopUserRegisterForm, ShopUserEditForm
from authapp.models import ShopUser


class Login(SuccessMessageMixin, UserPassesTestMixin, LoginView):
    """docstring for LoginView"""
    template_name = 'authapp/login.html'
    success_message = _('Log in success')
    form_class = ShopUserLoginForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Log in')
        if not settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY:
            context['google_in_closed'] = 'disabled'
        if not settings.SOCIAL_AUTH_VK_OAUTH2_KEY:
            context['vk_in_closed'] = 'disabled'
        return context

    def test_func(self):
        return not self.request.user.is_authenticated

    def handle_no_permission(self):
        return HttpResponseRedirect('/')


class Logout(LogoutView):
    """docstring for logoutView"""
    success_message = _('Log out success')

    def get_next_page(self):
        next_page = super().get_next_page()
        if next_page:
            messages.success(self.request, self.success_message)
        return next_page


class CreateProfile(SuccessMessageMixin, CreateView):
    """docstring for RegisterProfile"""
    template_name = 'authapp/register.html'
    model = ShopUser
    form_class = ShopUserRegisterForm
    success_url = '/'
    success_message = _('Registration completed successfully')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Register')
        return context

    def form_valid(self, form):
        dirname = settings.EMAIL_FILE_PATH
        filenames = os.listdir(dirname)
        filepaths = [os.path.join(dirname, filename) for filename in filenames]

        self.success_message = _('A confirmation letter has been sent to your email. '
                                 f'<a href="/{filepaths[-1]}">(Click for view)<a>')

        return super().form_valid(form)


class EditProfile(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """docstring for EditProfile"""
    template_name = 'authapp/edit.html'
    login_url = reverse_lazy('authapp:login')
    model = ShopUser
    form_class = ShopUserEditForm
    success_url = '/'
    success_message = _('Profile changed')

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit')
        return context


class DeleteProfile(DeleteView):
    """docstring for DelView"""
    model = ShopUser


class PasswordChange(LoginRequiredMixin, SuccessMessageMixin, PasswordChangeView):
    template_name = 'authapp/password_change_form.html'
    success_message = _('Password changed')
    success_url = reverse_lazy('authapp:edit')


class QuickLogin(SuccessMessageMixin, RedirectView):
    """quick login to the system"""
    pattern_name = 'mainapp:main'
    success_message = _('Log in success')

    def get_redirect_url(self, *args, **kwargs):
        slug = self.kwargs['slug']
        user = auth.authenticate(username=slug, password=slug)
        auth.login(self.request, user)
        messages.success(self.request, self.success_message)
        return super().get_redirect_url()


class VerifyRedirect(SuccessMessageMixin, RedirectView):
    pattern_name = 'mainapp:main'
    success_message = _('Thank you for registration, account is confirmed')

    def get_redirect_url(self, *args, **kwargs):
        activation_key = kwargs.get('activation_key')
        user = get_object_or_404(ShopUser, username=kwargs.get('username'))

        if user.activation_key == activation_key and not user.is_activation_key_expired():
            user.is_active = True
            user.save()
            auth.login(self.request, user, backend='django.contrib.auth.backends.ModelBackend',)
        else:
            self.success_message = _('Sorry, but activation key is invalid')

        messages.success(self.request, self.success_message)
        return super().get_redirect_url()

