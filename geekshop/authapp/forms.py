from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import UserChangeForm
from .models import ShopUser

from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.hashers import make_password


class MutualWidget:
    """Class for forms MutualWidgets """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.help_text = ''


class ShopUserLoginForm(MutualWidget, AuthenticationForm):
    class Meta:
        model = ShopUser
        fields = '__all__'


class ShopUserRegisterForm(MutualWidget, UserCreationForm):
    class Meta:
        model = ShopUser
        fields = ('username', 'first_name', 'password1', 'password2', 'email', 'age', 'avatar')

    def send_verify_mail(self, data, activation_key):
        verify_link = reverse_lazy('authapp:verify', args=[data['username'], activation_key])

        title = f'Verification for {data["username"]}'
        message = f'To confirm your account {data["username"]} click on the link:\
                      \n{settings.DOMAIN_NAME}{verify_link}'

        return send_mail(title, message, settings.EMAIL_HOST_USER, [data['email']], fail_silently=True, )

    def clean(self):
        activation_key = make_password(None)
        send_ver_mail = self.send_verify_mail(self.cleaned_data, activation_key)

        if send_ver_mail:
            user = super().save(commit=False)
            user.is_active = False
            user.activation_key = activation_key
            user.save()
        else:
            self.add_error(None, _('Error sending message to confirm registration. Please try later.'))

        return super().clean()


class ShopUserEditForm(MutualWidget, UserChangeForm):
    class Meta:
        model = ShopUser
        fields = ('username', 'first_name', 'email', 'age', 'avatar', 'password', 'status',)

    def __init__(self, *args, **kwargs):
        super(ShopUserEditForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = forms.HiddenInput()
        self.fields['status'].widget.attrs['disabled '] = True

