$(document).ready(function () {
    clr_field_avatar();
    change_style_file();
});


function clr_field_avatar() {
    var lbl_avatar = $('label[for="id_avatar"]');
    var input_avatar = $('input[name="avatar"]');

    lbl_avatar.closest('p').remove();
    $('<p class="avatar">').insertBefore('input[type="submit"]');

    lbl_avatar.clone().appendTo($(".avatar"));
    input_avatar.clone().appendTo($(".avatar"));
}

function change_style_file() {
    $(":file").filestyle({
        buttonBefore: true,
    });
    $('.buttonText').closest('label').addClass('btn-default');
    $('.buttonText').text("Choose image");
};
