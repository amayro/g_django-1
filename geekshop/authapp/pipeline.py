from os import path
from urllib.parse import urlparse

import wget
from django.contrib.messages import add_message
from social_core.exceptions import AuthForbidden

from geekshop import settings


def save_user_profile(backend, user, response, *args, **kwargs):
    """Save data for user from social network"""

    if backend.name == "google-oauth2":
        if 'ageRange' in response.keys():
            minAge = response['ageRange']['min']
            print(f'{user} minAge: {minAge}')
            if int(minAge) < 18:
                user.delete()
            raise AuthForbidden('social_core.backends.google.GoogleOAuth2')
        ph_url = response.get('image').get('url')
        image = path.basename(urlparse(ph_url).path)

    if backend.name == "vk-oauth2":
        ph_url = response.get('photo')
        image = path.basename(urlparse(ph_url).path)

    if user.avatar.name == 'users_avatars/default_avatar.jpg':
        user.avatar = wget.download(ph_url, f'.{settings.MEDIA_URL}users_avatars/{image}')
        user.avatar.name = f'users_avatars/{path.basename(urlparse(user.avatar.name).path)}'
        user.save()

    add_message(request=kwargs['request'], level=40, message='Log in with social network success')
