from django.contrib import admin
from .models import ShopUser, UserStatus
from django.utils.translation import ugettext_lazy as _


class UserStatusAdmin(admin.ModelAdmin):
    list_display = ('status', 'description',)


class ShopUserAdmin(admin.ModelAdmin):
    list_display = ('username', 'get_status', 'is_active', 'is_staff', 'is_superuser')

    def get_status(self, obj):
        return obj.status.get_status_display()

    get_status.short_description = _('Customer status')


admin.site.register(UserStatus, UserStatusAdmin)
admin.site.register(ShopUser, ShopUserAdmin)
