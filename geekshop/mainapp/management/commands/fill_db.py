from django.core.management.base import BaseCommand
from mainapp.models import MenuPage, ProductCategory, Product, Picture, Contact, Price
from django.contrib.auth.models import User
from authapp.models import ShopUser

import json, os


JSON_PATH = 'mainapp/json'


def loadFromJSON(file_name):
    with open(os.path.join(JSON_PATH, file_name + '.json'), 'r') as infile:
        return json.load(infile)


class Command(BaseCommand):
    help = 'Fill DB new data'

    def handle(self, *args, **options):
        print('--- Creating Pages  ---')
        main_pages = loadFromJSON('menu_pages')

        MenuPage.objects.all().delete()
        for page in main_pages:
            new_page = MenuPage(**page)
            new_page.save()

        print('--- Creating Product Categories ---')
        categories = loadFromJSON('categories')

        ProductCategory.objects.all().delete()
        for category in categories:
            new_category = ProductCategory(**category)
            new_category.save()

        print('--- Creating Products  ---')
        products = loadFromJSON('products')

        Product.objects.all().delete()
        Picture.objects.all().delete()
        Price.objects.all().delete()
        for product in products:
            category_name = product["category"]
            # Получаем категорию по имени
            _category = ProductCategory.objects.get(name=category_name)
            # Заменяем название категории объектом
            product['category'] = _category

            new_product = Product(category=product['category'],
                                  name=product['name'],
                                  short_desc=product['short_desc'],
                                  description=product['description'],
                                  quantity=product['quantity'],
                                  sort=product['sort'] if 'sort' in product else 0)
            new_product.save()

            for picture in product['image']:
                new_picture = Picture(name=product["name"], image=picture)
                new_picture.save()
                new_product.pictures.add(new_picture)

            new_price = Price(name=product["name"],
                              price_all=product['price_all'],
                              price_user=product['price_user'] if 'price_user' in product else '777')
            new_price.save()
            new_product.prices.add(new_price)

        print('--- Creating Contacts  ---')
        contacts = loadFromJSON('contacts')

        Contact.objects.all().delete()
        for contact in contacts:
            new_contact = Contact(**contact)
            new_contact.save()



        print('--- Creating User  ---')
        ShopUser.objects.all().delete()
        # Создаем суперпользователя при помощи менеджера модели
        super_user = ShopUser.objects.create_superuser('root', 'root@mail.ru', 'root')
        

