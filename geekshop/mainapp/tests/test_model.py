from django.core.management import call_command
from django.test import TestCase

from mainapp.models import *


class TestCoreModel(TestCase):
    """Test Model Core"""
    fixtures = ['test_db.json']

    def setUp(self):
        self.obj_model = Core

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp', )

    def test_get_queryset(self):
        objs = self.obj_model.objects.get_queryset()
        for obj in objs:
            self.assertTrue(obj.active)

    def test_delete(self):
        objs = self.obj_model.objects.get_all_queryset()
        count_before = objs.count()

        for obj in objs:
            obj.delete()
            self.assertFalse(obj.active)

        objs.delete()
        self.assertEqual(objs.count(), count_before)

    def test_hard_delete(self):
        objs = self.obj_model.objects.get_all_queryset()
        count_before = objs.count()

        obj = objs.first()
        obj.hard_delete()
        self.assertEqual(objs.count(), count_before-1)

        Order.objects.all().delete()
        objs.hard_delete()
        self.assertFalse(objs)


class TestProductModel(TestCase):
    """Test Model Product"""
    fixtures = ['test_db.json']

    def setUp(self):
        self.obj_model = Product

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp', )

    def test_get_products_slider_1(self):
        objs = self.obj_model.objects.get_products_slider_1()
        for obj in objs:
            self.assertEqual(obj.kind, obj.SLIDER_1)

    def test_get_products_slider_2(self):
        objs = self.obj_model.objects.get_products_slider_2()
        for obj in objs:
            self.assertEqual(obj.kind, obj.SLIDER_2)

    def test_get_products_featured(self):
        objs = self.obj_model.objects.get_products_featured()
        for obj in objs:
            self.assertEqual(obj.kind, obj.DEFAULT)

    def test_get_product_related(self):
        objs = self.obj_model.objects
        for obj in objs.all():
            response = objs.get_product_related(obj)
            self.assertNotIn(obj, response)

            for resp in response:
                self.assertEqual(resp.category, obj.category)

    def test_get_products_incategory(self):
        objs = ProductCategory.objects.all()
        for obj in objs:
            response = self.obj_model.objects.get_products_incategory(obj)
            for resp in response:
                self.assertEqual(resp.category, obj)


class TestInvoiceModel(TestCase):
    """Test Model Invoice"""
    fixtures = ['test_db.json']

    def setUp(self):
        self.user = ShopUser.objects.create_user(username='user_1', password='user_1', email='user_1@example.ru')
        self.prod_1 = Product.objects.all()[0]
        self.prod_2 = Product.objects.all()[2]
        self.inv = self.user.invoices.create(active=False)

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp', )

    def test_get_total_count(self):
        self.assertEqual(self.inv.get_total_count, 0)

        self.inv.orders.create(ware=self.prod_1, count=2)
        self.inv.orders.create(ware=self.prod_2, count=1)
        self.assertEqual(self.inv.get_total_count, 3)

    def test_get_total_cost(self):
        self.inv.orders.create(ware=self.prod_1, count=3)
        self.inv.orders.create(ware=self.prod_2, count=7)
        cost_p1 = self.prod_1.prices.get_price_user(self.user).price_user * 3
        cost_p2 = self.prod_2.prices.get_price_user(self.user).price_user * 7
        tcost = cost_p1 + cost_p2
        self.assertEqual(self.inv.get_total_cost, float(tcost))

