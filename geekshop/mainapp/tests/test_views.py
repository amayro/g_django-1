from decimal import Decimal

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse_lazy

from mainapp.context_processors import get_base_data
from mainapp.models import *


class TestView(TestCase):
    """smoke test views"""
    fixtures = ['test_db.json']

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp',)

    def test_IndexView(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainapp/index.html')

    def test_ErrorView(self):
        response = self.client.get(reverse_lazy('mainapp:showroom'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainapp/error.html')

    def test_ContactView(self):
        response = self.client.get(reverse_lazy('mainapp:contacts'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainapp/contact_list.html')

    def test_SearchView(self):
        response = self.client.get(reverse_lazy('mainapp:search'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainapp/search.html')

    def test_CatalogView(self):
        response = self.client.get(reverse_lazy('mainapp:catalog'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mainapp/product_list.html')

        for obj in ProductCategory.objects.all():
            response = self.client.get(reverse_lazy('mainapp:category', kwargs={'href': obj.href}))
            self.assertEqual(response.status_code, 200)

    def test_ProductView(self):
        for obj in Product.objects.all():
            response = self.client.get(reverse_lazy('mainapp:product', kwargs={'pk': obj.pk}))
            self.assertEqual(response.status_code, 200)

    def test_InvoiceView(self):
        response = self.client.get(reverse_lazy('mainapp:basket'))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith(str(reverse_lazy('authapp:login'))))

        for obj in ProductCategory.objects.all():
            response = self.client.get(reverse_lazy('mainapp:edit_order', kwargs={'ware': obj.pk}))
            self.assertEqual(response.status_code, 302)


class TestIndexView(TestCase):
    """tests IndexView"""
    fixtures = ['test_db.json']

    @classmethod
    def setUpTestData(cls):
        cls.user_t1 = ShopUser.objects.create_user(username='user_1', first_name='John',
                                                   password='user_1', email='user_1@example.ru')
        cls.user_t2 = ShopUser.objects.create_user(username='user_2',
                                                   password='user_2', email='user_2@example.ru')

    def setUp(self):
        self.client.force_login(self.user_t1)

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp',)

    def test_hello_name(self):
        response = self.client.get('/')
        self.assertNotContains(response, f'Hello, {self.user_t1.username}')
        self.assertContains(response, f'Hello, {self.user_t1.first_name}')

        self.client.force_login(self.user_t2)
        response = self.client.get('/')
        self.assertContains(response, f'Hello, {self.user_t2.username}')

    def test_context_data(self):
        response = self.client.get(reverse_lazy('mainapp:new'))
        que_1 = response.context_data['products_featORnew']
        que_2 = Product.objects.get_products_new()[:4]
        self.assertListEqual(list(que_1), list(que_2))

        for i in range(10):
            response = self.client.get(reverse_lazy('mainapp:new'))
            q1 = response.context_data['products_trend_1']
            q2 = response.context_data['products_trend_2']
            self.assertTrue(set(q1).isdisjoint(set(q2)))

            q1 = response.context_data['products_exclusive_1']
            q2 = response.context_data['products_exclusive_2']
            self.assertTrue(set(q1).isdisjoint(set(q2)))


class TestInvoiceView(TestCase):
    """tests InvoiceView"""
    fixtures = ['test_db.json']

    @classmethod
    def setUpTestData(cls):
        cls.user_t1 = ShopUser.objects.create_user(username='user_1', password='user_1', email='user_1@example.ru')
        cls.user_t2 = ShopUser.objects.create_user(username='user_2', password='user_2', email='user_2@example.ru')

    def setUp(self):
        self.client.force_login(self.user_t1)

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp',)

    def test_basket(self):
        """test invoice exists; add, edit, delete orders and confirm invoice"""

        """test invoice exists and add order"""
        response = self.client.get(reverse_lazy('mainapp:basket'))
        self.assertContains(response, 'No orders')
        self.assertIsNone(response.context_data['object'])

        products = Product.objects.all()[:10]
        for obj in products:
            self.client.post(reverse_lazy('mainapp:edit_order', kwargs={'ware': obj.pk}))

        response = self.client.get(reverse_lazy('mainapp:basket'))
        invoice_number = response.context_data['object'].pk
        context_pr = get_base_data(response._request)
        self.assertTrue(response.context_data['object'])
        self.assertEqual(context_pr['basket_count'], len(products))

        """check basket of another user"""
        self.client.logout()
        self.client.force_login(self.user_t2)
        response = self.client.get(reverse_lazy('mainapp:basket'))
        self.assertIsNone(response.context_data['object'])
        self.assertContains(response, 'No orders')
        self.assertEqual(context_pr['basket_count'], 0)

        """test edit order"""
        self.client.logout()
        self.client.force_login(self.user_t1)
        self.client.post(reverse_lazy('mainapp:edit_order', kwargs={'ware': obj.pk}), follow=True)
        self.client.post(reverse_lazy('mainapp:edit_order', kwargs={'ware': obj.pk}), follow=True)
        self.assertEqual(context_pr['basket_count'], len(products)+2)

        self.client.post(reverse_lazy('mainapp:edit_order', kwargs={'ware': obj.pk}), follow=True,
                         data={'count_down': ''})
        self.assertEqual(context_pr['basket_count'], len(products)+1)

        self.client.post(reverse_lazy('mainapp:del_order', kwargs={'ware': obj.pk}), follow=True)
        self.assertEqual(context_pr['basket_count'], len(products)-1)

        """test confirm order"""
        response = self.client.post(reverse_lazy('mainapp:edit_invoice'), follow=True)
        self.assertIsNone(response.context_data['object'])
        self.assertContains(response, f'Your order number №{invoice_number}')
        self.assertEqual(context_pr['basket_count'], 0)

    def test_status_invoice(self):
        self.assertFalse(self.user_t1.invoices.exists())

        product = Product.objects.all().first()
        for _ in range(2):
            response = self.client.post(reverse_lazy('mainapp:edit_order', kwargs={'ware': product.pk}), follow=True)
            inv = response.context_data['object']
            self.assertEqual(inv.status, inv.FORMING)
            self.client.post(reverse_lazy('mainapp:edit_invoice'), follow=True)

        self.assertEqual(len(self.user_t1.invoices.all()), 2)

        inv_confirm = self.user_t1.invoices.all().last()
        self.assertEqual(inv.id, inv_confirm.id)
        self.assertEqual(inv_confirm.status, inv.SENT_TO_PROCEED)


class TestProductView(TestCase):
    """tests ProductView"""
    fixtures = ['test_db.json']

    @classmethod
    def setUpTestData(cls):
        cls.user_t1 = ShopUser.objects.create_user(username='user_1', password='user_1', email='user_1@example.ru')
        cls.staff_t1 = ShopUser.objects.create_user(username='manager_1',
                                                    password='manager_1',
                                                    email='user_1@example.ru',
                                                    is_staff=True,
                                                    status=UserStatus.objects.get(status=UserStatus.GOLD))
        cls.test_products = Product.objects.all()[:20]

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp', )

    def test_name_prices(self):
        """test object name, price"""

        for obj in self.test_products:
            response = self.client.get(reverse_lazy('mainapp:product', kwargs={'pk': obj.pk}))
            self.assertContains(response, obj.prices.get_price_user(self.user_t1).price_user)

        self.client.force_login(self.user_t1)
        for obj in self.test_products:
            response = self.client.get(reverse_lazy('mainapp:product', kwargs={'pk': obj.pk}))
            self.assertContains(response, obj.name)
            self.assertContains(response, obj.prices.get_price_user(self.user_t1).price_user)

        self.client.logout()
        self.client.force_login(self.staff_t1)
        for obj in self.test_products:
            response = self.client.get(reverse_lazy('mainapp:product', kwargs={'pk': obj.pk}))
            self.assertContains(response, obj.name)
            self.assertContains(response, obj.prices.get_price_user(self.staff_t1).price_user)

    def test_staff_option(self):
        self.client.force_login(self.user_t1)
        for obj in self.test_products:
            response = self.client.get(reverse_lazy('mainapp:product', kwargs={'pk': obj.pk}))
            self.assertNotContains(response, 'EDIT PRODUCT')

            response = self.client.get(reverse_lazy('mainapp:edit_product', kwargs={'pk': obj.pk}))
            self.assertEqual(response.status_code, 302)
            self.assertTrue(response.url == '/')

        response = self.client.get(reverse_lazy('mainapp:add_product'))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url == '/')

        self.client.logout()
        self.client.force_login(self.staff_t1)
        for obj in self.test_products:
            response = self.client.get(reverse_lazy('mainapp:product', kwargs={'pk': obj.pk}))
            self.assertContains(response, 'EDIT PRODUCT')

            response = self.client.get(reverse_lazy('mainapp:edit_product', kwargs={'pk': obj.pk}))
            self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse_lazy('mainapp:add_product'))
        self.assertEqual(response.status_code, 200)

    def test_product_create_edit(self):
        """test for product create and edit"""

        """test product create"""
        self.client.force_login(self.staff_t1)
        response = self.client.get(reverse_lazy('mainapp:add_product'))
        data_post = {'name': 'test_product',
                     'short_desc': 'any text',
                     'kind': Product.DEFAULT,
                     'quantity': '0',
                     'views': '0',
                     }
        for i, status in enumerate(UserStatus.objects.all()):
            data_post.update({
                f'prices-{i}-price_base': '17',
                f'prices-{i}-group': f'{status.pk}',
                f'prices-{i}-core_ptr': '',
                f'prices-{i}-related_obj': '',
            })
            if status == self.staff_t1.status:
                price_status_idx = i

        management = response.context_data['prices'].management_form._bound_fields_cache
        for field in management.items():
            data_post[field[1].html_name] = field[1].initial

        response = self.client.post(reverse_lazy('mainapp:add_product'), data=data_post, follow=True,)
        product = Product.objects.all().last()
        self.assertEqual(response._request.path, reverse_lazy('mainapp:product', kwargs={'pk': product.pk}))

        success_message = response._request._messages._loaded_data
        self.assertTrue(success_message)
        self.assertEqual(success_message[0].message, 'Product added')
        self.assertEqual(response.context_data['object'].name, data_post['name'])

        prices = response.context_data['object'].prices.all()[0]
        self.assertEqual(prices.price_base, Decimal(data_post['prices-0-price_base']))

        """test product edit"""
        response = self.client.get(reverse_lazy('mainapp:edit_product', kwargs={'pk': product.pk}))
        prices = response.context_data['object'].prices.all()[0]
        data_post.update({
            'name': 'product_name_change',
            f'prices-{price_status_idx}-price_base': '20',
        })
        for i, status in enumerate(UserStatus.objects.all()):
            data_post.update({
                f'prices-{i}-core_ptr': str(prices.core_ptr_id),
                f'prices-{i}-related_obj': str(product.id),
            })

        management = response.context_data['prices'].management_form._bound_fields_cache
        for field in management.items():
            data_post[field[1].html_name] = field[1].initial

        response = self.client.post(reverse_lazy('mainapp:edit_product', kwargs={'pk': product.pk}),
                                    data=data_post, follow=True, )
        success_message = response._request._messages._loaded_data
        self.assertTrue(success_message)
        self.assertEqual(success_message[0].message, 'Product edited')
        self.assertEqual(response.context_data['object'].name, data_post['name'])

        prices = response.context_data['object'].prices.get_price_user(self.staff_t1)
        self.assertEqual(prices.price_base, Decimal(data_post[f'prices-{price_status_idx}-price_base']))

    def test_product_form_invalid(self):
        self.client.force_login(self.staff_t1)
        response = self.client.get(reverse_lazy('mainapp:add_product'))
        data_post = {'name': '',
                     'short_desc': 'any text',
                     'kind': Product.DEFAULT,
                     'quantity': '0',
                     'views': '0',
                     }
        for i, status in enumerate(UserStatus.objects.all()):
            data_post.update({
                f'prices-{i}-price_base': '-7',
                f'prices-{i}-group': f'{status.pk}',
                f'prices-{i}-core_ptr': '',
                f'prices-{i}-related_obj': '',
            })
        management = response.context_data['prices'].management_form._bound_fields_cache
        for field in management.items():
            data_post[field[1].html_name] = field[1].initial

        response = self.client.post(reverse_lazy('mainapp:add_product'), data=data_post, follow=True,)
        errors_price = list(filter(lambda x: bool(x) is True, response.context_data['prices']._errors))
        error_name = response.context_data['form']._errors['name'][0]
        error_price = errors_price[0]
        self.assertEqual(error_name, 'The field must not be empty')
        self.assertEqual(error_price, 'Prices must be greater than 0')


