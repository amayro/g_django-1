from django.core.management import call_command
from django.test import TestCase

from authapp.models import ShopUser
from mainapp.context_processors import get_base_data
from mainapp.models import Product


class TestContextProcessor(TestCase):
    """Test Context Processor"""
    fixtures = ['test_db.json']

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp', 'authapp', )

    def test_get_base_data(self):
        user_1 = ShopUser.objects.create_user(username='user_1', password='user_1', email='user_1@example.ru')
        user_2 = ShopUser.objects.create_user(username='user_2', password='user_2', email='user_2@example.ru')
        prod_1 = Product.objects.all()[0]
        prod_2 = Product.objects.all()[1]

        user = user_1
        self.client.force_login(user)

        response = self.client.get('/')
        context_pr = get_base_data(response.wsgi_request)
        self.assertEqual(context_pr['basket_count'], 0)

        inv = user.invoices.create(active=False)
        inv.orders.create(ware=prod_1, count=2)
        inv.orders.create(ware=prod_2, count=1)
        response = self.client.get('/')
        context_pr = get_base_data(response.wsgi_request)
        self.assertEqual(context_pr['basket_count'], 3)

        user = user_2
        self.client.force_login(user)

        response = self.client.get('/')
        context_pr = get_base_data(response.wsgi_request)
        self.assertEqual(context_pr['basket_count'], 0)

        inv = user.invoices.create(active=False)
        inv.orders.create(ware=prod_1, count=3)
        inv.orders.create(ware=prod_2, count=4)
        response = self.client.get('/')
        context_pr = get_base_data(response.wsgi_request)
        self.assertEqual(context_pr['basket_count'], 7)

        user = user_1
        self.client.force_login(user)
        response = self.client.get('/')
        context_pr = get_base_data(response.wsgi_request)
        self.assertEqual(context_pr['basket_count'], 3)
