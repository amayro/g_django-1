from django.contrib import admin
from .models import *
from django.utils.translation import ugettext_lazy as _


class EditDatabaseAdmin(admin.ModelAdmin):
    """Full Queryset with 'active=False' and hard_delete for superuser"""

    def get_queryset(self, request):
        manager = self.model._default_manager
        if getattr(manager, 'get_all_queryset', None):
            return manager.get_all_queryset()
        else:
            return manager.get_queryset()

    def delete_model(self, request, obj):
        """ Given a model instance delete it from the database."""
        if request.user.is_superuser and getattr(obj, 'hard_delete', None):
            return obj.hard_delete()
        return obj.delete()

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        if request.user.is_superuser and getattr(queryset, 'hard_delete', None):
            return queryset.hard_delete()
        return queryset.delete()


class PictureInline(admin.TabularInline):
    model = Picture
    fk_name = 'related_obj'
    fields = ('name', 'image', 'active', 'sort')


class PriceInline(admin.TabularInline):
    model = Price
    max_num = 4
    fk_name = 'related_obj'
    fields = ('price_base', 'group', 'price_user')
    readonly_fields = ('price_user',)


class OrderInline(admin.TabularInline):
    model = Order
    fk_name = 'invoices'
    field = ('ware', 'counts', 'invoices', 'add_datetime')


class ProductCategoryAdmin(EditDatabaseAdmin):
    list_display = ('name', 'active', 'sort')
    list_editable = ('active', 'sort')


class PictureAdmin(EditDatabaseAdmin):
    list_display = ('name', 'get_image_name', 'get_related_name', 'kind', 'active', 'sort')
    fields = ('name', 'image', 'active', 'kind', 'sort')
    list_editable = ('active', 'sort', 'kind',)

    def get_image_name(self, obj):
        pic_name_split = str(obj.image).split('/')[1]
        return pic_name_split
    get_image_name.short_description = _('Image filename')

    def get_related_name(self, obj):
        return obj.related_obj.name
    get_related_name.short_description = _('Related Product')


class ProductAdmin(EditDatabaseAdmin):
    list_display = ('name', 'category', 'get_discount', 'quantity',
                    'active', 'sort', 'kind', 'views')
    list_editable = ('category', 'quantity', 'active', 'sort', 'kind', 'views')
    inlines = (PriceInline, PictureInline,)
    fields = ('name', 'category', 'kind', 'short_desc', 'description', 'quantity', 'active', 'sort', 'views')
    list_select_related = ('category',)

    def get_discount(self, obj):
        return getattr(obj.category, 'discount', 0)

    get_discount.short_description = _('Discount, %')


class PriceAdmin(EditDatabaseAdmin):
    list_display = ('name', 'price_base', 'get_related_name')
    inlines = (PriceInline,)
    fields = ('name', 'price_base',)

    def get_related_name(self, obj):
        return obj.related_obj.name

    get_related_name.short_description = _('Related Product')


class ContactsAdmin(EditDatabaseAdmin):
    list_display = ('country', 'sity', 'phone', 'email', 'address', 'active')
    fields = ('country', 'sity', 'phone', 'email', 'address', 'active', 'sort')


class MenuPageAdmin(EditDatabaseAdmin):
    list_display = ('name', 'href', 'active')
    fields = ('name', 'href', 'active')


class OrderAdmin(EditDatabaseAdmin):
    list_display = ('ware', 'count', 'invoices', 'add_datetime', 'active')


class InvoiceAdmin(EditDatabaseAdmin):
    list_display = ('user',  'active', 'status', 'date_order')
    inlines = (OrderInline,)
    list_editable = ('status',)


admin.site.register(ProductCategory, ProductCategoryAdmin)
admin.site.register(Picture, PictureAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Price, PriceAdmin)
admin.site.register(Contact, ContactsAdmin)
admin.site.register(MenuPage, MenuPageAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Invoice, InvoiceAdmin)


