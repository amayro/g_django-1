from django.core.validators import MaxValueValidator
from django.db import models
from django.db.models import Sum, F, FloatField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from authapp.models import ShopUser, UserStatus


class CoreQuerySet(models.QuerySet):
    """CoreQuerySet need for change initial QuerySet;
     realization soft delete for QuerySet - filter().delete()"""

    def delete(self):
        return super(CoreQuerySet, self).update(active=False)

    def hard_delete(self):
        return super(CoreQuerySet, self).delete()


class CoreManager(models.Manager):
    """change initial QuerySet and queryset for superuser in admin_panel"""

    def get_queryset(self):
        return CoreQuerySet(self.model).filter(active=True)

    def get_all_queryset(self):
        return CoreQuerySet(self.model)


class Core(models.Model):
    """docstring for Core; realization soft delete for object - get().delete()"""
    name = models.CharField(_('name'), max_length=250, blank=True)
    description = models.TextField(_('description'), blank=True)
    sort = models.IntegerField(_('sort'), default=0, null=True, blank=True)
    active = models.BooleanField(_('active'), default=True)
    all_objects = models.Manager()
    objects = CoreManager()

    def __str__(self):
        return f'{self.name}'

    @property
    def verbose_name(self):
        return self._meta.verbose_name

    @property
    def verbose_name_plural(self):
        return self._meta.verbose_name_plural

    def delete(self):
        self.active = False
        self.save()

    def hard_delete(self):
        super().delete()


class MenuPage(Core):
    """docstring for MenuPages"""
    class Meta:
        verbose_name = _('Main page')
        verbose_name_plural = _('Main pages')

    href = models.CharField(_('page url'), max_length=64, blank=False)
    objects = CoreManager()


class ProductCategoryManager(CoreManager):
    """docstring for ProductCategoryManager"""
    def get_category(self):
        return self.order_by('sort')


class ProductCategory(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Product category')
        verbose_name_plural = _('Product categories')

    href = models.CharField(_('page url'), max_length=64, blank=False)
    discount = models.PositiveIntegerField(verbose_name="Discount, %", default=0, validators=[MaxValueValidator(100),])
    objects = ProductCategoryManager()


class PictureManager(CoreManager):
    """docstring for  PictureManager"""

    def get_queryset(self):
        return self.get_all_queryset()

    def get_main(self):
        return self.filter(kind=self.model.DEFAULT, active=True)

    def get_inactive(self):
        return self.filter(active=False)

    def get_slider(self):
        return self.filter(kind=self.model.SLIDER, active=True)


class Picture(Core):
    """docstring for Picture"""
    class Meta:
        verbose_name = _('Picture')
        verbose_name_plural = _('Pictures')
        ordering = ('sort',)

    image = models.ImageField(upload_to='products_images')
    related_obj = models.ForeignKey(Core, verbose_name=_(u'pictures'), null=True, blank=True, related_name='pictures',
                                    on_delete=models.CASCADE)
    objects = PictureManager()

    DEFAULT = 'dft'
    SLIDER = 'sld'

    Picture_KIND_CHOICES = (
        (DEFAULT, 'Default'),
        (SLIDER, 'Picture for slider'),
    )

    kind = models.CharField(max_length=3, choices=Picture_KIND_CHOICES, default=DEFAULT)


class ProductManager(CoreManager):
    """docstring for ProductManager"""

    def get_products_slider_1(self):
        return self.filter(kind=self.model.SLIDER_1).order_by('?')

    def get_products_slider_2(self):
        return self.filter(kind=self.model.SLIDER_2).order_by('?')

    def get_products_trend(self):
        return list(self.order_by('-views', '?').select_related('category'))

    def get_products_exclusive(self):
        return list(self.filter(kind=self.model.EXCLUSIVE).order_by('?'))

    def get_products_featured(self):
        return self.filter(kind=self.model.DEFAULT).order_by('?').select_related('category')

    def get_products_new(self):
        return self.order_by('-pk').select_related('category')

    def get_product_related(self, obj):
        return self.exclude(id=obj.id).filter(category=obj.category).order_by('?').select_related('category')

    def get_products_incategory(self, href):
        return self.filter(category__href=href)


class Product(Core):
    """docstring for Product"""
    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        ordering = ('name',)

    category = models.ForeignKey(ProductCategory, null=True, blank=True, on_delete=models.SET_NULL)
    short_desc = models.CharField(verbose_name="Short description", max_length=200)
    quantity = models.PositiveIntegerField(verbose_name="Quantity in stock", default=0)
    views = models.PositiveIntegerField(verbose_name="Views", default=0)
    objects = ProductManager()

    DEFAULT = 'dft'
    SLIDER_1 = 'sl1'
    SLIDER_2 = 'sl2'
    EXCLUSIVE = 'exl'

    Product_KIND_CHOICES = (
        (DEFAULT, 'Default'),
        (SLIDER_1, 'Product in the 1st slider'),
        (SLIDER_2, 'Product in the 2nd slider'),
        (EXCLUSIVE, 'Exclusive product'),
        )

    kind = models.CharField(max_length=3, choices=Product_KIND_CHOICES, default=DEFAULT)

    def up_views(self):
        self.views = F('views') + 1
        self.save()


class PriceManager(CoreManager):
    """docstring for ProductManager"""

    def get_price_user(self, user):
        user_status = getattr(getattr(user, 'status', None), 'status', UserStatus.DEFAULT)
        return self.filter(group__status=user_status).first()


class Price(Core):
    """
    Prices for products. Fields:
    price_base - this is base price which input by manager
    price_user - this is real price which takes into account the base price and discount
    group - user customer status
    """
    class Meta:
        verbose_name = _('Price')
        verbose_name_plural = _('Prices')

    price_base = models.DecimalField(verbose_name="Product price, $", max_digits=8, decimal_places=2, default=0)
    price_user = models.DecimalField(verbose_name="Price with discount, $", max_digits=8, decimal_places=2, default=0)
    group = models.ForeignKey(UserStatus, verbose_name=_(u'For user with status'), null=False, related_name='prices',
                              on_delete=models.CASCADE)
    related_obj = models.ForeignKey(Core, verbose_name=_(u'prices'), null=True, blank=True, related_name='prices',
                                    on_delete=models.CASCADE)
    objects = PriceManager()

    def __str__(self):
        return f'{self.price_base}'

    @receiver(post_save, sender=ProductCategory)
    def update_changed_discount(sender, instance, **kwargs):
        """update price_user after changed discount in ProductCategory"""

        for product in Product.objects.filter(category=instance):
            list(map(lambda x: x.save(), product.prices.all()))

    def save(self, *args, **kwargs):
        discount = getattr(self.related_obj.category, 'discount', 0)
        self.price_user = self.price_base if discount == 0 else self.price_base * (100-discount) / 100
        return super().save(*args, **kwargs)


class Contact(Core):
    """docstring for Contacts"""
    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

    country = models.CharField(_('country'), max_length=64, blank=False)
    sity = models.CharField(_('sity'), max_length=64, blank=False)
    phone = models.CharField(_('phone'), max_length=32, blank=False)
    email = models.CharField(_('email'), max_length=32, blank=False)
    address = models.CharField(_('address'), max_length=250, blank=False)
    objects = CoreManager()


class InvoiceManager(models.Manager):
    """docstring for InvoiceManager"""

    def get_invoice(self):
        """this is invoice in customer's basket"""

        return self.filter(active=False).select_related('user__status').\
            prefetch_related('orders__ware__category', 'orders__ware__prices__group')

    def get_last_user_invoice(self, request):
        return self.filter(user=request.user.id).last()

    def get_staff_invoice(self, pk):
        """need only for select_related for page customer's invoice for staff"""

        return self.filter(pk=pk).select_related('user__status')

    def get_staff_invoices(self):
        return self.exclude(status=self.model.FORMING).order_by('-pk').select_related('user')

    def get_staff_invoices_count(self):
        return self.filter(status=self.model.SENT_TO_PROCEED).count()


class Invoice(Core):
    """docstring for Invoice"""
    class Meta:
        verbose_name = _('Invoice')
        verbose_name_plural = _('Invoices')

    user = models.ForeignKey(ShopUser, null=False, blank=True, related_name='invoices', on_delete=models.PROTECT)
    date_order = models.DateTimeField(verbose_name=_('Time of order'), auto_now_add=True)
    objects = InvoiceManager()

    FORMING = 'FM'
    SENT_TO_PROCEED = 'STP'
    PAID = 'PD'
    PROCEEDED = 'PRD'
    READY = 'RDY'
    CANCEL = 'CNC'

    Invoice_STATUS_CHOICES = (
        (FORMING, 'Is forming'),
        (SENT_TO_PROCEED, 'Sent to proceed'),
        (PAID, 'Invoice paid'),
        (PROCEEDED, 'Invoice proceed'),
        (READY, 'Ready to issue'),
        (CANCEL, 'Cancel'),
        )

    status = models.CharField(max_length=3, choices=Invoice_STATUS_CHOICES, default=FORMING)

    @property
    def get_total_count(self):
        return self.orders.get_total_count() or 0

    @property
    def get_total_cost(self):
        return round(self.orders.get_total_cost(), 2)


class OrderManager(models.Manager):
    """docstring for InvoiceManager"""

    def get_total_count(self):
        return self.aggregate(Sum('count'))['count__sum']

    def get_total_cost(self, order=None):
        if order:
            self = self.filter(pk=order.pk, ware__prices__group=order.invoices.user.status)
        else:
            self = self.filter(ware__prices__group=self.all().first().invoices.user.status)

        cost = Sum(F('ware__prices__price_user') * F('count'), output_field=FloatField())
        return self.aggregate(summ=cost)['summ']


class Order(Core):
    """docstring for Order"""
    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    ware = models.ForeignKey(Product, related_name='orders', null=True, blank=True, on_delete=models.PROTECT)
    count = models.PositiveIntegerField(verbose_name=_('Counts'), default=0, blank=True)
    invoices = models.ForeignKey(Invoice, related_name='orders', null=True, blank=True, on_delete=models.CASCADE)
    add_datetime = models.DateTimeField(verbose_name=_('Time of addition'), auto_now_add=True)
    objects = OrderManager()

    @property
    def get_total_cost(self):
        return self._meta.model.objects.get_total_cost(order=self)
