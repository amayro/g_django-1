from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView, DetailView, UpdateView, TemplateView, CreateView, DeleteView

from mainapp.forms import ProductForm, ProductCategoryForm, PictureForm, PriceFormSet, \
                          StaffInvoiceForm, StaffOrderFormSet
from .models import *


def add_links_href_app(lst_lst_name_href, app='mainapp'):
    """setattr 'href_app' for links_menu_products without category
    "lst_lst_name" - list should look like: [[name_page_1, name_url_1], [name_page_2, name_url_2], [etc.]]"""
    new_list = []
    for link in lst_lst_name_href:
        new_list.append({'name': f'{link[0]}', 'href': f'{link[1]}', 'href_app': f'{app}:{link[1]}'})
    return new_list


class UserStaffTest(UserPassesTestMixin):
    """docstring for test user is staff"""
    def test_func(self):
        return self.request.user.is_staff

    def handle_no_permission(self):
        return HttpResponseRedirect('/')


class IndexView(ListView):
    """docstring for page home"""
    template_name = 'mainapp/index.html'
    model = Product

    def get_queryset(self):
        response = super().get_queryset()
        if 'new' == self.request.resolver_match.url_name:
            self.kwargs['products_featORnew'] = Product.objects.get_products_new()[:4]
        else:
            self.kwargs['products_featORnew'] = Product.objects.get_products_featured()[:4]

        return response, self.kwargs

    def post(self, request, *args, **kwargs):
        if self.request.is_ajax():
            super().get(request, *args, **kwargs)
            context = {'products': self.kwargs['products_featORnew'],
                       'class': 'bbA_img'}
            context = render_to_string('mainapp/includes/inc_products.html', context)
            response = JsonResponse({'result': context})

            return response
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        products_trend = Product.objects.get_products_trend()
        products_exclusive = Product.objects.get_products_exclusive()

        context.update({'caption': _('Home Interior'),
                        'links_no_cat': add_links_href_app([['Featured', 'main'], ['New', 'new']]),
                        'products_featORnew': self.kwargs['products_featORnew'],
                        'products_slider_1': Product.objects.get_products_slider_1(),
                        'products_slider_2': Product.objects.get_products_slider_2(),
                        'products_trend_1': products_trend[:6],
                        'products_trend_2': products_trend[6:10],
                        'products_exclusive_1': products_exclusive[:2],
                        'products_exclusive_2': products_exclusive[2:3],
                        })
        return context


class CatalogView(ListView):
    """docstring for page catalog"""
    model = Product
    paginate_by = 6
    ordering = ('id',)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Products')
        context.update({'links_no_cat': add_links_href_app([['All', 'catalog']]),
                        'links_menu_products': ProductCategory.objects.get_category(),
                        'products_exclusive': Product.objects.get_products_exclusive(),
                        })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax():
            self.template_name = 'mainapp/includes/inc_categories.html'

        return super().render_to_response(context, **response_kwargs)

    def get_queryset(self):
        response = super().get_queryset()
        if 'href' in self.kwargs:
            href = self.kwargs.get('href', '')
            response = Product.objects.get_products_incategory(href)
        return response.select_related('category')


class ProductDetail(DetailView):
    """docstring for page product"""
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Product')

        obj = self.object
        obj.up_views()
        obj_prices = obj.prices.get_price_user(self.request.user)
        context.update({
            'object_list': Product.objects.get_product_related(obj),
            'price_without_discount': obj_prices.price_base,
            'price_now': obj_prices.price_user,
        })
        return context


class ProductCreate(UserStaffTest, SuccessMessageMixin, CreateView):
    """docstring for add product"""
    model = Product
    form_class = ProductForm
    success_message = _('Product added')

    def get_success_url(self):
        pk = self.object[0].related_obj.pk
        return reverse_lazy('mainapp:product', kwargs={'pk': pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Add product')
        formset = PriceFormSet()
        context['prices'] = formset
        if self.request.POST:
            context['prices'] = PriceFormSet(self.request.POST)

        statuses = UserStatus.objects.all()
        context['prices'].extra = len(statuses)
        for i, form in enumerate(formset.forms):
            form.initial['group'] = statuses[i]
        return context

    def form_valid(self, form):
        formset = PriceFormSet(self.request.POST)

        if not formset.is_valid():
            return self.form_invalid(form)
        else:
            formset.instance = form.save()
            return super().form_valid(formset)


class ProductUpdate(UserStaffTest, SuccessMessageMixin, UpdateView):
    """docstring for edit product"""
    model = Product
    form_class = ProductForm
    success_message = _('Product edited')

    def get_success_url(self):
        return reverse_lazy('mainapp:product', args=[self.kwargs['pk']])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Edit product')
        context['prices'] = PriceFormSet(instance=self.get_object())
        if self.request.POST:
            context['prices'] = PriceFormSet(self.request.POST, instance=self.get_object())
        context['prices'].extra = 0
        return context

    def form_valid(self, form):
        formset = PriceFormSet(self.request.POST, instance=self.get_object())

        if not formset.is_valid():
            return self.form_invalid(form)
        else:
            formset.instance = form.save()
            return super().form_valid(formset)


class ProductDelete(UserStaffTest, SuccessMessageMixin, DeleteView):
    """docstring for delete product"""
    model = Product
    success_message = _('Product deleted')

    def get_success_url(self):
        messages.success(self.request, self.success_message)
        return reverse_lazy('mainapp:catalog')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = 'Delete product'
        return context


class PictureCreate(UserStaffTest, SuccessMessageMixin, CreateView):
    """docstring for add product"""
    model = Picture
    form_class = PictureForm
    success_message = _('Picture added')

    def form_valid(self, form):
        form.instance.related_obj_id = self.kwargs.get('related_obj')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('mainapp:edit_product', args=(self.object.related_obj.id or 0,))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['related_obj'] = self.kwargs.get('related_obj')
        context['caption'] = 'Add picture'
        return context


class PictureUpdate(UserStaffTest, SuccessMessageMixin, UpdateView):
    """docstring for edit product"""
    model = Picture
    form_class = PictureForm
    success_message = _('Picture edited')

    def get_success_url(self):
        return reverse_lazy('mainapp:edit_product', args=(self.object.related_obj.id or 0,))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = 'Edit picture'
        return context


class PictureDelete(UserStaffTest, SuccessMessageMixin, DeleteView):
    """docstring for delete product"""
    model = Picture
    success_message = _('Picture marked as inactive')

    def get_success_url(self):
        messages.success(self.request, self.success_message)
        return reverse_lazy('mainapp:product',  kwargs={'pk': self.get_object().related_obj.id})

    def delete(self, request, *args, **kwargs):
        success_url = self.get_success_url()
        self.get_object().delete()
        return HttpResponseRedirect(success_url)


class ProductCategoryCreate(UserStaffTest, SuccessMessageMixin, CreateView):
    """docstring for add product"""
    model = ProductCategory
    form_class = ProductCategoryForm
    success_message = _('Category added')

    def get_success_url(self):
        return reverse_lazy('mainapp:category', kwargs={'href': self.object.href})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Add category')
        return context


class ProductCategoryUpdate(UserStaffTest, SuccessMessageMixin, UpdateView):
    """docstring for edit product"""
    model = ProductCategory
    slug_field = 'href'
    form_class = ProductCategoryForm
    success_message = _('Category edited')

    def get_success_url(self):
        return reverse_lazy('mainapp:category', kwargs={'href': self.object.href})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = 'Edit category'
        return context


class ProductCategoryDelete(UserStaffTest, SuccessMessageMixin, DeleteView):
    """docstring for delete product"""
    template_name = 'mainapp/product_confirm_delete.html'
    model = ProductCategory
    slug_field = 'href'
    success_url = reverse_lazy('mainapp:catalog')
    success_message = _('Category deleted')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Delete category')
        return context

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        messages.success(self.request, self.success_message)

        Product.objects.filter(category__pk=self.object.id).delete()
        self.object.hard_delete()
        return HttpResponseRedirect(success_url)


class InvoiceCreate(LoginRequiredMixin, CreateView):
    """docstring for invoices"""
    fields = ('user',)
    model = Invoice
    login_url = reverse_lazy('authapp:login')
    success_url = reverse_lazy('mainapp:basket')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.success_url)

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.status = self.model.FORMING
        form.instance.active = False
        return super().form_valid(form)


class InvoiceDetail(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    """docstring for invoices"""
    model = Invoice
    slug_field = 'user'
    queryset = model.objects.get_invoice()
    login_url = reverse_lazy('authapp:login')
    success_url = reverse_lazy('mainapp:basket')

    def get_object(self):
        self.kwargs[self.slug_url_kwarg] = self.request.user
        try:
            response = super().get_object()
        except:
            response = None
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'caption': 'Basket',
            'invoice_number': getattr(self.model.objects.get_last_user_invoice(self.request), 'pk', None),
        })
        return context


class InvoiceUpdate(InvoiceDetail, UpdateView):
    """docstring for invoices"""
    fields = ('user',)
    success_message = _('Order is accepted')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.success_url)

    def get_object(self):
        response = super().get_object()
        if not response:
            InvoiceCreate.as_view()(self.request, *self.args, **self.kwargs)
            response = super().get_object()
        return response

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.status = self.model.SENT_TO_PROCEED
        form.instance.date_order = now()
        form.instance.active = True
        Invoice.objects.get_last_user_invoice(self.request).orders.update(active=True)
        return super().form_valid(form)


class InvoiceDelete(InvoiceDetail, DeleteView):
    """docstring for DelView"""
    model = Invoice
    success_message = _('Basket is empty')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.success_url)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object and self.object.orders.count():
            messages.success(self.request, self.kwargs.get('success_message', ''))
            return HttpResponseRedirect(self.get_success_url())

        self.success_url = '/'
        messages.success(self.request, self.success_message)
        self.object.hard_delete()
        return HttpResponseRedirect(self.success_url)


class StaffInvoiceList(UserStaffTest, ListView):
    """Invoice list for staff"""
    model = Invoice
    queryset = Invoice.objects.get_staff_invoices()
    template_name = 'mainapp/invoice_staff_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Invoices')
        context['title'] = _('Customer orders')
        return context


class StaffInvoiceCreate(UserStaffTest, SuccessMessageMixin, CreateView):
    """docstring for create invoice"""
    form_class = StaffInvoiceForm
    model = Invoice
    template_name = 'mainapp/invoice_staff_form.html'
    success_message = _('Invoice created')

    def get_success_url(self):
        pk = self.object[0].invoices.core_ptr_id
        return reverse_lazy('mainapp:edit_staff_invoice', kwargs={'pk': pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Invoices')
        context['title'] = _('Create invoice')
        context['orders'] = StaffOrderFormSet()
        if self.request.POST:
            context['orders'] = StaffOrderFormSet(self.request.POST)
        context['orders'].extra = 0
        return context

    def form_valid(self, form):
        formset = StaffOrderFormSet(self.request.POST)

        if not formset.is_valid():
            return self.form_invalid(form)
        else:
            formset.instance = form.save()
            return super().form_valid(formset)


class StaffInvoiceUpdate(UserStaffTest, SuccessMessageMixin, UpdateView):
    """Customer Invoice update for staff"""
    model = Invoice
    template_name = 'mainapp/invoice_staff_form.html'
    form_class = StaffInvoiceForm
    success_message = _('Invoice edited')

    def get_success_url(self):
        return reverse_lazy('mainapp:edit_staff_invoice', args=[self.kwargs['pk']])

    def get_object(self):
        queryset = self.model.objects.get_staff_invoice(self.kwargs['pk'])
        return super().get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = self.object
        customer = obj.user
        full_name = f'{customer.first_name} {customer.last_name}'
        order_query = obj.orders.select_related('ware').prefetch_related('ware__prices__group')

        context.update({
            'caption': _('Invoices'),
            'title': _('Order №'),
            'customer_full_name': full_name if len(full_name) > 1 else '-',
            'customer_username': customer.username or '-',
            'customer_email': customer.email or '-',
            'customer_status': customer.status or '-',
            'orders':  StaffOrderFormSet(instance=obj, queryset=order_query),
        })

        if self.request.POST:
            context['orders'] = StaffOrderFormSet(self.request.POST, instance=obj)
        return context

    def form_valid(self, form):
        formset = StaffOrderFormSet(self.request.POST, instance=self.get_object())

        if not formset.is_valid():
            return self.form_invalid(form)
        else:
            formset.instance = form.save()
            return super().form_valid(formset)


def order_formset_ajax(request, invoice, num, ware):
    if request.is_ajax():
        inv = Invoice.objects.get(pk=invoice)
        formset = StaffOrderFormSet(instance=inv)
        formset.extra = num
        formset.forms[num].initial['ware'] = Product.objects.get(pk=ware)
        formset.forms[num].initial['count'] = 1
        content = {
            'form': formset.forms[num],
            'num': num,
            'object': True,
            'ajax_price': formset.forms[num].initial['ware'].prices.get_price_user(inv.user).price_user,
        }

        result = render_to_string('mainapp/includes/inc_order_formset.html', content)
        return JsonResponse({'result': result})


class OrderCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """docstring for create product"""
    fields = ('ware', 'invoices', 'count')
    model = Order
    slug_url_kwarg = slug_field = 'ware'
    login_url = reverse_lazy('authapp:login')
    success_url = reverse_lazy('mainapp:basket')
    success_message = _('Item added to basket')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.success_url)

    def form_valid(self, form):
        form.instance.invoices = InvoiceUpdate(request=self.request, args=self.args, kwargs=self.kwargs).get_object()
        ware = form.fields[self.slug_url_kwarg].to_python(self.kwargs[self.slug_url_kwarg])
        form.instance.ware = ware
        form.instance.active = False

        return super().form_valid(form)


class OrderDetail(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    """docstring for OrderView"""
    model = Order
    slug_url_kwarg = slug_field = 'ware'
    login_url = reverse_lazy('authapp:login')
    success_url = reverse_lazy('mainapp:basket')

    def get_object(self):
        try:
            response = super().get_object(queryset=self.get_queryset())
        except:
            response = None
        return response

    def get_queryset(self):
        inv = InvoiceUpdate(request=self.request, args=self.args, kwargs=self.kwargs).get_object()
        return super().get_queryset().filter(invoices=inv)


class OrderUpdate(OrderDetail, UpdateView):
    """docstring for AddView"""
    fields = ('count',)
    success_message_1 = _('The quantity of product has been reduced to %s')
    success_message_2 = _('The quantity of product has been increased to %s')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.success_url)

    def get_object(self):
        response = super().get_object()
        if not response:
            OrderCreate.as_view()(self.request, *self.args, **self.kwargs)
            response = super().get_object()
        return response

    def form_valid(self, form):
        if 'count_down' in self.request.POST:
            count = form.instance.count = form.instance.count - 1
            self.success_message = self.success_message_1 % count if count != 0 else ''
        else:
            count = form.instance.count = form.instance.count + 1
            self.success_message = self.success_message_2 % count if count > 1 else ''
        response = super().form_valid(form)

        if self.request.is_ajax():
            context = {'count': self.object.count,
                       'message': self.success_message,
                       'total_summ': self.object.invoices.get_total_count,
                       'total_cost': self.object.invoices.get_total_cost,}
            messages.get_messages(self.request).used = True
            response = JsonResponse(context)

        if count <= 0:
            OrderDelete.as_view()(self.request, *self.args, **self.kwargs)
        return response


class OrderDelete(OrderDetail, DeleteView):
    """docstring for DelView"""
    success_message = _('Item has been removed from the basket')

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.success_url)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object:
            if 'delete' or 'count_down' in self.request.POST:
                self.object.hard_delete()
                self.kwargs['success_message'] = self.success_message
                return InvoiceDelete.as_view()(self.request, *self.args, **self.kwargs)
            return super().get(request, *args, **kwargs)
        return HttpResponseRedirect(self.get_success_url())


class ContactView(ListView):
    """docstring for page contacts"""
    model = Contact


class SearchView(ListView):
    """docstring for search"""
    model = Product
    template_name = 'mainapp/search.html'

    def post(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        response = super().get_queryset()
        search = self.request.POST.get('search', '')
        search_in = self.request.POST.getlist('search_in')
        query_ask = Q()
        if search:
            if self.request.POST.get('search_in'):
                if 'category' in search_in:
                    query_ask |= Q(category__name__icontains=search)
                if 'name' in search_in:
                    query_ask |= Q(name__icontains=search) | Q(name__contains=search.capitalize()) # icontains НЕ работает для sqllite
                if 'description' in search_in:
                    query_ask |= Q(description__icontains=search)

        response = response.filter(query_ask) if query_ask else None
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = _('Search')
        if 'search' in self.request.POST:
            context['search_phrase'] = self.request.POST.get('search', '')
        return context


class ErrorView(TemplateView):
    """docstring for error"""
    template_name = 'mainapp/error.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['caption'] = 'Error'
        return context
