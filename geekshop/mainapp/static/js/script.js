'use strict';

$(document).ready(function () {
    $(".slider ").each(function () { // обрабатываем каждый слайдер

        const interval = 4; // интервал в секундах
        const obj = $(this);
        const elements = $(obj).find("li").length;

        $(obj).append("<div class='nav_slider'></div>");
        $(obj).find("li").each(function () {
            $(obj).find(".nav_slider").append("<span data-slide='" + $(this).index() + "'></span>"); // добавляем блок навигации
            $(this).attr("data-slide", $(this).index());
        });
        $(obj).find(".nav_slider span").first().addClass("on"); // делаем активным первый элемент меню

        function start_slider() {
            const loop_slider = setInterval(() => {
                const index = $(obj).find('.on').data("slide"),
                    nextIndex = index + 1 < elements ? index + 1 : 0;
                sliderJS(nextIndex, obj);
            }, interval * 1000);
            $(window).on("blur", () => clearInterval(loop_slider));
        }

        start_slider();
        $(window).on("focus", () => start_slider());
    });
});

function sliderJS(index, sl) { // slider function
    const ul = $(sl).find("ul"); // находим блок
    const bl = $(sl).find("li[data-slide=" + index + "]"); // находим любой из элементов блока
    const step = $(bl).width(); // ширина объекта

    $(sl).find("span").removeClass("on"); // убираем активный элемент
    $(sl).find("span[data-slide=" + index + "]").addClass("on"); // делаем активным текущий

    $(ul).animate({
        marginLeft: "-" + step * index
    }, 500); // 500 скорость перемотки
}

$(document).on("click", ".nav_slider span", function () { // slider click navigate
    const sl = $(this).closest(".slider"); // находим, в каком блоке был клик
    const index = $(this).data("slide");

    sliderJS(index, sl);
    return false;
});


$(document).on("click", ".new_item .line_menu .no_cat a", function (event) {
    event.preventDefault();
    const target_href = event.target;
    const href = $(this).attr('href');
    const new_item = $('.new_item');

    if (target_href) {
        $.ajax({
            data: $(this).closest('form').serialize(),
            type: "POST",
            url: href,

            success: function (data) {
                new_item.find('.bbA_img').stop(true, true);
                new_item.find('.block_img').stop(true, true);

                new_item.find('.block_img').height('287px');
                new_item.find('.block_img').animate({width: '0px',}, 500,);
                new_item.find('.bbA_img').animate({width: '0px',}, 500, function () {
                    new_item.find('.block_img').html(data.result);
                    new_item.find('.block_img').animate({width: '1170px',}, 500,);
                    new_item.find('.bbA_img').animate({width: '270px',}, 500,);
                    console.log('ajax done');
                });
            },
        });

        new_item.find(".line_menu .no_cat a").removeClass("active_menu");
        $(this).addClass("active_menu");
    }
});
