function editOrder() {
    var obj = arguments[0];
    var form = obj.closest('form');
    if (form && obj) {
        $.post(form.action, $(form).serialize(), function(data) {
            updateData(data, obj);
        });
    }
}

function updateData(data, obj) {
    if (data) {
        var message = $('body').find('ul.messages li').first();
        var count = $(obj).closest('div.group_c').find('p.count span');
        var total_cost = $(obj).closest('section.block_contact_maps').find('h2.total_cost span');
        var total_summ = $('ul.menu').find('span.basket_count');


        if (data.total_cost == 0) {
            success:  document.location.href = '/'
        }
        else if (data.count == 0) {
            location.reload();
            console.log('ajax done');
        }

        message.text(data.message)
        total_cost.text(data.total_cost)
        total_summ.text(data.total_summ)
        count.text(data.count)

    }

}

