window.onload = function () {
    var TOTAL_FORMS = parseInt($('input[name="orders-TOTAL_FORMS"]').val());

    function order_update(order_num, change_order_cost) {
        var order_total_cost;
        var count_field = document.getElementById('id_orders-' + order_num + '-count');
        var count = parseInt($(count_field).val()) || 0;
        var price = parseFloat($(count_field).closest('td').nextAll('.order-price').text().replace(',', '.')) || 0;

        order_total_cost = Number((price * count).toFixed(2));
        if (change_order_cost === undefined) {
            return {
                count: count,
                total_cost: order_total_cost
            }
        } else {
            $(count_field).closest('td').nextAll('.order-total_cost').html(order_total_cost.toString());
        }
    }

    function invoice_update() {
        var TOTAL_FORMS = parseInt($('input[name="orders-TOTAL_FORMS"]').val());
        var total_count = 0;
        var total_cost = 0;
        var count_visible_ord = 0;

        for (var i = 0; i < TOTAL_FORMS; i++) {
            var hidden_order = $('#id_orders-' + i + '-ware').closest('tr');

            if (hidden_order.attr('style') !== 'display: none;') {
                var order_data = order_update(i);
                total_count += order_data.count;
                total_cost += order_data.total_cost;
                count_visible_ord++;
            }
        }

        if (count_visible_ord == 1) {
            $('.delete-row').addClass('disabled')
        }

        total_cost = Number((total_cost).toFixed(2));

        $('.total_cost span').html(total_cost.toString());
        $('.total_count span').html(total_count.toString());
    }

    $('.table_invoice').on('change', 'select', function () {
        var target = event.target;
        var order_num = parseInt(target.name.replace('orders-', '').replace('-ware', ''));
        var invoice_num = $('#id_orders-0-invoices').val();
        var ware_pk = $(this).val();
        var current_select = $(this);

        if (ware_pk) {
            $.ajax({
                url: "/order_formset/" + invoice_num + "/" + order_num + "/" + ware_pk + "/",

                success: function (data) {
                    current_select.closest('tr').html(data.result);
                    console.log('ajax done');

                    $('#id_orders-' + order_num + '-ware').closest('tr').formset({
                        addText: '',
                        deleteText: '',
                        prefix: 'orders',
                        removed: invoice_update
                    });
                    move_field_delete(order_num);
                    $('.dynamic-form-add').next('.dynamic-form-add').remove();
                    invoice_update();
                }
            });
        } else {
            $('#id_orders-' + order_num + '-count').val(0);
            $(current_select).closest('td').nextAll('.order-price').html('0');
            order_update(order_num, true);
            invoice_update();
        }
    });

    $('.table_invoice').on('change', 'input[type="number"]', function () {
        var target = event.target;
        var order_num = parseInt(target.name.replace('orders-', '').replace('-count', ''));

        order_update(order_num, true);
        invoice_update();
    });

    $('.form_invoice').on('click', 'button[type="submit"]', function(){
        var TOTAL_FORMS = parseInt($('input[name="orders-TOTAL_FORMS"]').val());
        for (var i = 0; i < TOTAL_FORMS; i++) {
            var field_count = $('#id_orders-' + i + '-count');
            if (!field_count.val()) {
                field_count.val(0)
            }
        }
    });

    function open_field_delete() {
        $('.delete-row').removeClass('disabled').removeAttr('style');
    }

    function move_field_delete(order_num) {
        var field_delete = $('#id_orders-' + order_num + '-DELETE').unwrap();
        field_delete.nextAll('.delete_row').append(field_delete);
    }


    for (var i = 0; i < TOTAL_FORMS; i++) {
        move_field_delete(i)
    }

    $('.formset_row').formset({
        addText: '',
        deleteText: '',
        prefix: 'orders',
        added: open_field_delete,
        removed: invoice_update
    });
};