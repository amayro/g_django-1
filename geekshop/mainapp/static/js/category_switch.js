window.onload = () => {
    $(document).on('click', '.line_menu a', event => switchCategory(event));
    $(document).on('click', '.pagination a', event => switchCategory(event));

    function switchCategory(event) {
        if (event.target.hasAttribute('href')) {
            const link = event.target.href;
            const link_array = link.split('/');
            console.log(link_array);
            if (link_array[3] === 'catalog') {
                $.ajax({
                    url: link,
                    success: function (data) {
                        $('.trend').html(data);
                    },
                });
                event.preventDefault();
            }
        }
    }
};

