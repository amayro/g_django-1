# Generated by Django 2.1.1 on 2018-10-05 08:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0008_auto_20181005_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='invoices',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='orders', to='mainapp.Invoice'),
        ),
    ]
