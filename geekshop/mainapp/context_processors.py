from .models import MenuPage, Invoice
from django.conf import settings


def get_base_data(request, response={}):
    response['VERSION'] = settings.VERSION
    response['current_url'] = request.resolver_match.url_name
    response['links_menu'] = []
    for menu in MenuPage.objects.all():
        response['links_menu'] += [menu]
        if hasattr(menu, 'href'):
            setattr(menu, 'href_app', f'mainapp:{menu.href}')

    inv = Invoice.objects.get_last_user_invoice(request)
    response['basket_count'] = inv.get_total_count if not getattr(inv, 'active', True) else 0
    response['invoices_count'] = Invoice.objects.get_staff_invoices_count()

    return response



