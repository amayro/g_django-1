from django.urls import path, re_path
import mainapp.views as mainapp
import os
from pathlib import PurePath

app_name = PurePath(os.path.dirname(__file__)).parts[-1]

urlpatterns = [
    path('', mainapp.IndexView.as_view(), name='main'),
    path('new/', mainapp.IndexView.as_view(), name='new'),

    path('catalog/', mainapp.CatalogView.as_view(), name='catalog'),
    path('catalog/product_<int:pk>/', mainapp.ProductDetail.as_view(), name='product'),
    path('catalog/<str:href>/', mainapp.CatalogView.as_view(), name='category'),
    path('showroom/', mainapp.ErrorView.as_view(), name='showroom'),
    path('contacts/', mainapp.ContactView.as_view(), name='contacts'),
    path('search/', mainapp.SearchView.as_view(), name='search'),

    path('category/add/', mainapp.ProductCategoryCreate.as_view(), name='add_category'),
    path('category/edit/<slug:slug>', mainapp.ProductCategoryUpdate.as_view(), name='edit_category'),
    path('category/delete/<slug:slug>', mainapp.ProductCategoryDelete.as_view(), name='del_category'),

    path('product/add/', mainapp.ProductCreate.as_view(), name='add_product'),
    path('product/edit/<int:pk>', mainapp.ProductUpdate.as_view(), name='edit_product'),
    path('product/delete/<int:pk>', mainapp.ProductDelete.as_view(), name='del_product'),

    path('picture/add/<int:related_obj>', mainapp.PictureCreate.as_view(), name='add_picture'),
    path('picture/edit/<int:pk>', mainapp.PictureUpdate.as_view(), name='edit_picture'),
    path('picture/del/<int:pk>', mainapp.PictureDelete.as_view(), name='del_picture'),

    path('orders/add_<int:ware>', mainapp.OrderCreate.as_view(), name='add_order'),
    path('orders/edit_<int:ware>', mainapp.OrderUpdate.as_view(), name='edit_order'),
    path('orders/del_<int:ware>', mainapp.OrderDelete.as_view(), name='del_order'),

    path('basket/', mainapp.InvoiceDetail.as_view(), name='basket'),
    path('invoice/confirm/', mainapp.InvoiceUpdate.as_view(), name='edit_invoice'),

    path('staff/invoices/', mainapp.StaffInvoiceList.as_view(), name='staff_invoices'),
    path('staff/invoice/create/', mainapp.StaffInvoiceCreate.as_view(), name='add_staff_invoice'),
    path('staff/invoice/<int:pk>', mainapp.StaffInvoiceUpdate.as_view(), name='edit_staff_invoice'),
    path('order_formset/<int:invoice>/<int:num>/<int:ware>/', mainapp.order_formset_ajax, name='order_formset'),

]
