from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from .models import *


class ProductForm(forms.ModelForm):
    """Form for product"""

    class Meta:
        model = Product
        fields = ('name', 'category', 'kind', 'short_desc', 'description', 'quantity', 'views',)
        labels = {
            'kind': _('Type'),
        }

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('name'):
            self.add_error('name', _('The field must not be empty'))
        return cleaned_data


class PriceForm(forms.ModelForm):
    """Form for price"""

    class Meta:
        model = Price
        fields = ('group', 'price_base', )
        labels = {
            'group': _('Status'),
            'price_base': _('Price, $:'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['group'].widget.attrs['readonly'] = True


class PriceInlineFormSet(forms.BaseInlineFormSet):
    """Inlineformset for prices"""

    def clean(self):
        for data in self.cleaned_data:
            if data.get('price_base', 0) <= 0:
                self._errors.append('Prices must be greater than 0')

        if self._errors:
            raise ValidationError(self._errors)

        return self.cleaned_data


PriceFormSet = forms.inlineformset_factory(Product, Price, form=PriceForm, formset=PriceInlineFormSet,
                                           fk_name='related_obj', can_delete=False, extra=1)


class ProductCategoryForm(forms.ModelForm):
    """Form for productCategory"""

    class Meta:
        model = ProductCategory
        fields = '__all__'
        help_texts = {
            'sort': _('Order of the category in the menu'),
            'href': _('Enter the relative name of the page\'s url<br>'
                      'Example: modern_2018<br>'
                      'It\'s look like as: http:/site/catalog/modern_2018/'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['href'].widget.attrs['placeholder'] = _('modern_2018')


class PictureForm(forms.ModelForm):
    """Form for Picture"""

    class Meta:
        model = Picture
        fields = ('image', 'name', 'kind', 'sort', 'active',)
        labels = {
            'kind': _('Type'),
            'image': 'Picture',
        }
        help_texts = {
            'sort': _('Picture position relative to others'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs['instance']:
            self.fields.pop('image')


class StaffInvoiceForm(forms.ModelForm):
    """Form for product"""

    class Meta:
        model = Invoice
        fields = ('user', 'status', 'description',)
        labels = {
            'description': _('Comment'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs['instance']:
            self.fields['user'].widget = forms.HiddenInput()

    def clean(self):
        if self.cleaned_data['status'] == self.Meta.model.FORMING:
            self.add_error('status', _('You can\'t select this status'))
        if self.cleaned_data['user'] is None:
            self.add_error('user', _('Select user'))

        return self.cleaned_data


class StaffOrderForm(forms.ModelForm):
    """Form for price"""

    class Meta:
        model = Order
        fields = ('ware', 'count',)
        labels = {
            'ware': 'Product'
        }


class StaffOrderInlineFormSet(forms.BaseInlineFormSet):
    """Inlineformset for Order"""

    def clean(self):
        cleaned_data = self.cleaned_data
        ware_is_none = not all(False if data['ware'] is None and not data['DELETE']
                               else True for data in cleaned_data if data)

        if not cleaned_data[0] or ware_is_none:
            self._errors.append('Add product or remove unused fields')
            raise ValidationError(self._errors)

        lst_ware = []
        for data in cleaned_data:
            if data.get('ware'):
                lst_ware.append(data['ware'].pk)
                if data.get('count', 0) is None:
                    self._errors.append('Product count must be integer')
                elif data.get('count', 1) <= 0:
                    self._errors.append('Product count must be greater than 0')

        if len(set(lst_ware)) != len(lst_ware):
            self._errors.append('You are trying to add a product that is already in the invoice.')

        if any(self._errors):
            raise ValidationError(self._errors)

        return cleaned_data

    def delete_existing(self, obj, commit=True):
        if commit:
            obj.hard_delete()


StaffOrderFormSet = forms.inlineformset_factory(Invoice, Order, form=StaffOrderForm, formset=StaffOrderInlineFormSet,
                                                fk_name='invoices', extra=1, min_num=1, validate_min=True)
